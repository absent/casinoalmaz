'use strict';

exports.config = {
    seleniumAddress: 'http://0.0.0.0:4444/wd/hub',
    capabilities: {
        browserName: 'phantomjs',
        'phantomjs.binary.path': './node_modules/phantomjs/bin/phantomjs'
    },
    baseUrl: 'http://localhost:3000',
    specs: ['tests/e2e/**/*.spec.js'],
    jasmineNodeOpts: {
        showColors: false
    }
};
