class PageUrl < ActiveRecord::Base
  PAGE_TYPES = [ :sport, :live, :custom, :system ]
  enum page_type: PAGE_TYPES

  validates_uniqueness_of :url

  has_many :page_contents, foreign_key: 'url_id', :dependent => :destroy

  has_many :sport_titles

end
