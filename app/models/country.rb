class Country < ActiveRecord::Base
  belongs_to :alias, foreign_key: 'code'

  def self.ordered_all_countries
    Country
        .where(:lang_code => I18n.locale)
        .order("case when code = 'RUS' then 1
                                  when code = 'BLR' then 2
                                  when code = 'UKR' then 3
                                  when code = 'AZE' then 4
                                  when code = 'ARM' then 5
                                  when code = 'GEO' then 6
                                  when code = 'KAZ' then 7
                                  when code = 'MDA' then 8
                                  when code = 'TJK' then 9
                                  when code = 'KGZ' then 10
                                  when code = 'UZB' then 11
                                  when code = 'TKM' then 12
                     end asc, name ASC")
  end

end
