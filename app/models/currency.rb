class Currency < ActiveRecord::Base
  def self.ordered_all_currencies
    Currency
        .where(country:AzartHall::Locale.cur)
        .order("case when code = 'RUB' then 1 end asc, name ASC")
  end
end
