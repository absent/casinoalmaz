module ApplicationHelper
  def root_link()
   return 'http://' + get_domain()
  end
  def get_domain
    Rails.env == 'production' ?  request.host : request.host_with_port
  end
end
