class GamesController < FrontEndController
  Container = 'casinoalmaz.com'

  def index
    render :index
  end

  def get
    if params[:category].nil?
      apps = Game.all
    else
      apps = game_by_category(params[:category])
    end
    processed = []
    apps.each do |app|
       processed << {
          id: app.id,
          name: app.name,
          img: "/images/#{app.img}.jpg",
          url: "http://#{Container}/#{app.app}",
          loader: app.loader
      }
    end
    render json: processed.compact
  end

  private
  def game_by_category(category_name)
    case category_name
    when 'popular'
      Game.where(id_name: ['e_mr', 'g_bor', 'g_llc', 'i_fc', 'g_bgb', 'i_cm'])
    when 'roulettes'
      Game.where(id_name: ['e_mr','bet_roul' ])
    when 'board'
      Game.where(id_name: ['e_sicboaus', 'e_sicbo', 'e_keno'])
    when 'live'
      Game.where(id_name: ['e_keno', 'bet_roul'])
    else
      Game.where(type_game: 'slot')
    end
  end
end
