class LocaleController < FrontEndController
  def switch
    I18n.locale = params[:code]
    redirect_to :back
  end
end
