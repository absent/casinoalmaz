require 'net/http'
require 'uri'

class AuthController < FrontEndController

  def index
    render :index
  end

  def login
    p params

    if params[:login].present? || params[:password].present?
      render json: AzartHall::Auth.check_auth(session, params[:login], params[:password])
    else
      render json: { status: 3, message: I18n.t('auth.not_filled') }
    end
  end

  def forgot_password
    if params[:email].present?
      render json: AzartHall::Auth.send_newpassword(params[:email], 1, session)
    else
      render json: { status: 3, message: I18n.t('auth.not_filled') }
    end
  end

  def resend_email
    host = request.protocol + request.host_with_port

    if params[:email].present?
      render json: AzartHall::Auth.resend_email(params[:email], host)
    else
      render json: { status: 3, message: I18n.t('auth.not_filled') }
    end
  end

  def forgot_login
    render :forgot_login
  end

  def resend_email
    render :resend_email
  end

  def logout
    AzartHall::Auth.clean_user_session(session)
    redirect_to '/', flash: {message: ''}
  end

end
