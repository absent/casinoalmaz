require 'net/http'
class TopController < FrontEndController
  TopUrl = URI.parse 'http://bl.onebett.com/api/leader/getTopWin?dayDiff=30'
  def get
    top = Net::HTTP.get TopUrl
    render json: top
  end
end
