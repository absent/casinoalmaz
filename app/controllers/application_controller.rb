class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_action :set_locale

  def set_locale
    res = AzartHall::Locale.init(cookies, request, params)
    cookies[:lng] = {value: res}
    if !params[:lng].nil?
      params.delete :lng
      redirect_to url_for(params.except(:lng).merge(only_path: false))
    end
  end

end
