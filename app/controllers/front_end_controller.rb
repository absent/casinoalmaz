class FrontEndController < ApplicationController
  before_filter :get_currency
  before_filter :get_params
  before_filter :update_balance

  def get_currency
    @currencies = Currency.ordered_all_currencies
  end

  def get_params
    url = PageUrl.find_by(:url => request.original_fullpath)
    @page_params = url.page_contents.find_by(:locale => I18n.locale) unless url.nil?
  end

  def update_balance
    if session[:guid]
      AzartHall::Auth.get_balance(session)
    end
  end
end
