require 'net/http'
class CurrencyController < FrontEndController
  def index
  end
  def get
    locale = I18n.locale
    currencies = Currency.select("id", "code", "name").where(country:locale)
    render json: currencies
  end
end
