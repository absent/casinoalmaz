class AccountController < ApplicationController
  layout 'account'
  before_action :check_user_auth
  skip_before_filter :verify_authenticity_token

  # profile methods
  def profile
    render 'profile'
  end

  def get_userinfo
    data = AzartHall::Account.get_userinfo(session[:guid])
    render json: {
      :country => data["CountryCode"],
      :address => data["Address"],
      :currency => data["CurrencyCode"],
      :date_of_birth => data["Dob"],
      :gender => data["Gender"],
      :firstname => data["FirstName"],
      :lastname => data["LastName"]
    }
  end

  def save_userinfo
    today = Time.new(
      Time.new.year,
      Time.new.month,
      Time.new.day
    )
    profile_dob = Date.parse(params[:date_of_birth])
    user_date = Time.new(
      profile_dob.year,
      profile_dob.month,
      profile_dob.day
    )

    if (TimeDifference.between(today, user_date).in_years) < 18
      render json: {:status => 5, :message => "#{I18n.t 'frontend.account.profile.errors.min_years'}"}
    else
      data = {
        :Dob => params[:date_of_birth],
        :LastName => params[:lastname],
        :FirstName => params[:firstname],
        :Gender => params[:gender],
        :CountryCode => params[:country],
        :Address => params[:address]
      }
      render json: AzartHall::Account.set_userinfo(session[:guid], data)
    end
  end


  # security methods
  def security
    render 'security'
  end

  def change_password
    data = {
      :UserId => session[:guid],
      :OldPassword => params[:current],
      :CheckOldPassword => true,
      :NewPassword => params[:new],
      :GenerateNewPassword => false
    }
    render json: AzartHall::Account.change_password(data)
  end

  # deposit methods
  def deposit
    @nickname = session[:guid]
    data = AzartHall::Crypto.receive(session[:guid], "#{request.protocol}#{request.host_with_port}/account/deposit_2btx")

    p data

    @input_address = data["input_address"]
    @qrcode_src = eval %Q{"#{data["qrcode_src"]}"}

    render 'deposit'
  end

  def deposit_2btx
    data = {
        :UserId => params[:userId],
        :PaymentMethodId => 27,
        :Amount => params[:value],
        :Description => '2btx transaction',
        :Close => true
    }

    render json: Azarthall::Account.deposit(data)
  end

  # withdrawal methods
  def withdrawal
    render 'withdrawal'
  end

  def payment_methods
    render json: AzartHall::Account.get_payment_methods(session[:guid])
  end

  def withdrawal_request
    data = {
      :UserId => session[:guid],
      :Type => 2,
      :Topic => "QBit Withdrawal request.",
      :Message =>
        "Метод вывода: #{params[:method]}, " \
        "Номер счета: #{params[:wallet]}, " \
        "Сумма: #{params[:amount]}"
    }

    render json: AzartHall::Account.withdrawal_request(data)
  end

  private
  def check_user_auth
    redirect_to root_path unless session[:username].present?
  end
end
