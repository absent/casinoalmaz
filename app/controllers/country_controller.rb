require 'net/http'
class CountryController < FrontEndController
  def index
  end
  def get
    locale = I18n.locale
    countries = Country
      .select("id", "code", "name")
      .where(lang_code: locale)
      .select { |country|  country.name != ''}
    render json: countries
  end
end
