class GameController < FrontEndController
  BaseURL = 'bl.onebett.com'
  CustomerID = 'akYQxsdZ'
  Container = 'casinoalmaz.com/media'
  def index
    accountId = "accountId=#{session[:guid]}"
    p params
    if params[:type] == "1"
      if !session[:guid]
        redirect_to registration_path
        return
      else
        token = Net::HTTP.get(BaseURL, "/api/leader/GetSlotSession?#{accountId}&type=1")
      end
    else
      token = Net::HTTP.get(BaseURL, "/api/leader/GetSlotSession?#{accountId}&type=0")
    end

    g = Game.find params[:id]
    @game = {
        :token => token,
        :path_to_storage => 'http://' + Container,
        :customer_id => CustomerID,
        :app => 'http://' + Container + '/' + g.app,
        :loader => g.loader
    }

    render :index
  end
end