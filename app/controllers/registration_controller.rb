class RegistrationController < FrontEndController
  before_filter :check_reg_user
  def index
    render :index
  end

  def registration
    host = request.protocol + request.host_with_port
    today = Time.new(Time.new.year, Time.new.month, Time.new.day)
    params[:currencycode] = 'BTC'
    render json: AzartHall::Registration.save_user(session, params, host)
  end


  def validate
    result = AzartHall::Registration.validate_user(params)
    case result["Status"]
      when 0
        redirect_to root_path, flash: {message: I18n.t('frontend.register.response.success')}
      when 1
        redirect_to root_path, flash: {message: I18n.t('frontend.register.response.already_verified')}
      when 2..3
        redirect_to '/forgot_password', flash: {message: I18n.t('frontend.register.response.fail')}
      else
        redirect_to root_path, flash: {message: I18n.t('frontend.register.response.error')}
    end
  end

  def success
    redirect_to root_path, flash: {message: I18n.t('frontend.register.response.success')}
  end

  def check_reg_user
    redirect_to root_path, :status => :moved_permanently unless session[:guid].nil?
  end
end
