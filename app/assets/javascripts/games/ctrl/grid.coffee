do (angular = window.angular, _ = window._) ->
  class GridCtrl
    constructor: ($scope, t, GamesService) ->
      categories =
        all: null
        slots: 'slots'
        roulettes: 'roulettes'
        live: 'live'
        board: 'board'

      $scope.categories = _.map _.keys(categories), (key) ->
        name: t.games.categories[key]
        code: categories[key]

      $scope.getGames = (container, param, callback=null) ->
        GamesService.get param, (response) ->
          $scope[container] = response
          callback() if callback?

      $scope.getByCategory = (category) ->
        $scope.getGames 'apps', category, ->
          $scope.isActive = category

      $scope.getByCategory()

      $scope.getGames 'popular', 'popular'

  GridCtrl.$inject = ['$scope', 't', 'GamesService']

  angular
    .module 'azarthall.games'
    .controller 'GridCtrl', GridCtrl
