do (angular = window.angular) ->
  class GamesService
    constructor: (@$http) ->
    get: (param, callback) ->
      params = { category: param } if param
      @$http.get '/get', params: params
        .success (response) =>
          callback(response)

  GamesService.$inject = ['$http']

  angular
    .module 'azarthall.games'
    .service 'GamesService', GamesService
