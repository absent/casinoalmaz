disableNgAnimate = ($animate) ->
  restrict: 'A'
  link: (scope, element)-> $animate.enabled false, element

disableNgAnimate.$inject = ['$animate']

angular
  .module 'azarthall.games'
  .directive 'disableNgAnimate', disableNgAnimate
