do (angular = window.angular) ->
  userDataFactory = ->
    is_auth: false
    data: ''

  angular
    .module 'azarthall.auth'
    .factory 'userData', userDataFactory
