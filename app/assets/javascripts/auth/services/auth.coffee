do (angular = window.angular) ->
  class AuthService
    constructor: (@$http) ->

    login: (data, callback) ->
      @$http.get '/auth', params: data
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка выполнения запроса'})

    password_recovery: (data, callback) ->
      @$http.get '/forgot_password', params: data
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка выполнения запроса'})

  AuthService.$inject = ['$http']

  angular
    .module 'azarthall.auth'
    .service 'AuthService', AuthService
