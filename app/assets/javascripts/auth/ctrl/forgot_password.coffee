do (angular = window.angular, _ = window._) ->
  class RecoveryModalCtrl
    constructor: ($scope, t, AuthService, userData, $modalInstance, $modal) ->
      $scope.form =
        email: ''

      $scope.cancel = ->
        $modalInstance.dismiss('cancel')

      $scope.sendRequest = ->
        AuthService.password_recovery $scope.form, (response) ->
          if response.status is 0
            $scope.cancel()
            $modal.open
              animation: false
              templateUrl: 'loginmodal'
              controller: 'AuthModalCtrl'
              size: 'sm'
              resolve:
                message: ->
                  t.auth.password_recovery
          else
            $scope.alert = response.message

  RecoveryModalCtrl.$inject = ['$scope', 't', 'AuthService', 'userData', '$modalInstance', '$modal']

  angular
    .module 'azarthall.auth'
    .controller 'RecoveryModalCtrl', RecoveryModalCtrl
