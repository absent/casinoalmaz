do (angular = window.angular, _ = window._) ->
  class AuthWindowCtrl
    constructor: ($scope, t, AuthService, userData, $modal) ->
      $scope.form =
        login   : ''
        password: ''

      $scope.login = ->
        AuthService.login $scope.form, (response) ->
          $scope.message = ''
          $scope.alert   = ''
          unless response.status in [3, 5, 1]
              userData.is_auth = true
              userData.data    = response.user_data
              window.location.replace '/'
          else
            $scope.alert = response.message

      $scope.forgotPassword = ->
        $modal.open
          animation: false
          templateUrl: 'reset_passwordform'
          controller: 'RecoveryModalCtrl'
          size: 'sm'

  AuthWindowCtrl.$inject = ['$scope', 't', 'AuthService', 'userData', '$modal']

  angular
    .module 'azarthall.auth'
    .controller 'AuthWindowCtrl', AuthWindowCtrl
