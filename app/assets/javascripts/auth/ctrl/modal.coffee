do (angular = window.angular, _ = window._) ->
  class AuthModalCtrl
    constructor: ($scope, t, AuthService, userData, $modalInstance, message, $modal) ->
      $scope.form =
        login   : ''
        password: ''

      $scope.message = message

      $scope.login = ->
        AuthService.login $scope.form, (response) ->
          $scope.message = ''
          $scope.alert   = ''
          unless response.status in [3, 5, 1]
              userData.is_auth = true
              userData.data    = response.user_data
              window.location.reload()
          else
            $scope.alert = response.message

      $scope.cancel = ->
        $modalInstance.dismiss('cancel')

      $scope.forgotPassword = ->
        $scope.cancel()
        $modal.open
          animation: false
          templateUrl: 'reset_passwordform'
          controller: 'RecoveryModalCtrl'
          size: 'sm'

  AuthModalCtrl.$inject = ['$scope', 't', 'AuthService', 'userData', '$modalInstance', 'message', '$modal']

  angular
    .module 'azarthall.auth'
    .controller 'AuthModalCtrl', AuthModalCtrl
