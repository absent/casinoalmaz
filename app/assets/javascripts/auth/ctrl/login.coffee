do (angular = window.angular, _ = window._) ->
  class AuthCtrl
    constructor: ($scope, t, $modal) ->
      $scope.openAuth = ->
        $modal.open
          animation: false
          templateUrl: 'loginmodal'
          controller: 'AuthModalCtrl'
          size: 'sm'
          resolve:
            message: ->
              ''
            items: ->
              $scope.form

  AuthCtrl.$inject = ['$scope', 't', '$modal']

  angular
    .module 'azarthall.auth'
    .controller 'AuthCtrl', AuthCtrl
