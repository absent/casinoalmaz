#= require_tree ./utils
#= require_tree ./registration
#= require_tree ./auth

do (angular = window.angular) ->
  angular
    .module('azarthall')
    .requires.push 'ui.bootstrap',
                   'azarthall.utils',
                   'azarthall.registration',
                   'azarthall.auth'
