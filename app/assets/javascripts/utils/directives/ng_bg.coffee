do (angular = window.angular) ->
    angular
      .module 'azarthall.utils'
      .directive 'ngBg', ->
        (scope, element, attrs) ->
            element.css
                'background-image': "url(#{attrs.ngBg})"
