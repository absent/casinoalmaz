do (angular = window.angular, _ = window._) ->
    angular
      .module 'azarthall.utils'
      .filter 'regex', ->
        (text, regex) ->
          if text
            patt = new RegExp regex
            result = _.filter text, (ch) ->
              patt.test ch
            result.join ''
