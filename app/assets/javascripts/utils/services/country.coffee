do (angular = window.angular) ->
  class CountryService
    constructor: (@$http) ->
    get: (callback) ->
      @$http.get '/countries'
        .success (response) =>
          callback(response)

  CountryService.$inject = ['$http']

  angular
    .module 'azarthall.utils'
    .service 'CountryService', CountryService
