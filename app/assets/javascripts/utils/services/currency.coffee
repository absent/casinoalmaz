do (angular = window.angular) ->
  class CurrencyService
    constructor: (@$http) ->
    get: (callback) ->
      @$http.get '/currencies'
        .success (response) =>
          callback(response)

  CurrencyService.$inject = ['$http']

  angular
    .module 'azarthall.utils'
    .service 'CurrencyService', CurrencyService
