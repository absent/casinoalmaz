do (angular = window.angular) ->
  class TopService
    constructor: (@$http) ->
    get: (callback) ->
      @$http.get '/top'
        .success (response) =>
          callback(response)

  TopService.$inject = ['$http']

  angular
    .module 'azarthall.utils'
    .service 'TopService', TopService
