do (angular = window.angular) ->
  class TopCtrl
    constructor: ($scope, TopService) ->
      do ->
        TopService.get (response) ->
          $scope.topList = _.map response, (player) ->
            name: player.UserName
            won: player.WinAmount

  TopCtrl.$inject = ['$scope', 'TopService']

  angular
    .module 'azarthall.utils'
    .controller 'TopCtrl', TopCtrl
