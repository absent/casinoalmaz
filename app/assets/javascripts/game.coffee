#= require_tree ./games

do (angular = window.angular) ->
  angular
    .module('azarthall')
    .requires.push 'ui.bootstrap',
                   'azarthall.games'
