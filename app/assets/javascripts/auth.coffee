#= require_tree ./auth

do (angular = window.angular) ->
  angular
    .module('azarthall')
    .requires.push 'ui.bootstrap',
                   'azarthall.auth'
