#= require_tree ./registration

do (angular = window.angular) ->
  angular
    .module('azarthall')
    .requires.push 'ui.bootstrap',
                   'azarthall.registration'
