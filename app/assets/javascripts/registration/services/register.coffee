do (angular = window.angular) ->
  class RegisterService
    constructor: (@$http) ->
    register: (data, callback) ->
      @$http.post '/register', data
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка выполнения запроса'})

  RegisterService.$inject = ['$http']

  angular
    .module 'azarthall.registration'
    .service 'RegisterService', RegisterService
