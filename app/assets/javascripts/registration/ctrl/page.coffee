do (angular = window.angular, _ = window._) ->
  class RegisterWindowCtrl
    constructor: ($scope, t, RegisterService, CurrencyService, $modal) ->
      # get avaliable currencies
      do ->
        CurrencyService.get (response) ->
          $scope.currencyList = response

      $scope.form =
        username: ''
        email   : ''
        password: ''

      $scope.currencyText = t.register.choose

      $scope.confirm_password = ''
      $scope.ageOfConsent     = no

      $scope.chooseCurrency = (name, code) ->
        $scope.currencyText  = name
        $scope.form.currency = code

      $scope.validate = ->
        valid = (email) ->
          valid = /\S+@\S+\.\S+/
          valid.test email
        switch
          # all fields are filled
          when not _.every $scope.form
            $scope.alert = t.register.validation.all_filled
            no
          # email is ok
          when not valid $scope.form.email
            $scope.alert = t.register.validation.valid_email
            no
          # password longer than 7
          when not ($scope.form.password.length > 7)
            $scope.alert = t.register.validation.password_length
            no
          # password matches confirmation
          when not ($scope.form.password is $scope.confirm_password)
            $scope.alert = t.register.validation.password_confirmed
            no
          # age of consent checked
          when not $scope.ageOfConsent
            $scope.alert = t.register.validation.age_confirmed
            no
          else yes

      $scope.register = ->
        if $scope.validate()
          RegisterService.register $scope.form, (response) ->
            unless response.status is 0
              $scope.alert = response.message
            else
              $scope.message = t.auth.confirm_account

  RegisterWindowCtrl.$inject = ['$scope', 't', 'RegisterService', 'CurrencyService', '$modal']

  angular
    .module 'azarthall.registration'
    .controller 'RegisterWindowCtrl', RegisterWindowCtrl
