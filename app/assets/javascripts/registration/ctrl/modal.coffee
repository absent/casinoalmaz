do (angular = window.angular, _ = window._) ->
  class RegisterModalCtrl
    constructor: ($scope, t, RegisterService, CurrencyService, $modalInstance, $modal) ->
      # get avaliable currencies
      do ->
        CurrencyService.get (response) ->
          $scope.currencyList = response

      $scope.form =
        username: ''
        email   : ''
        password: ''

      $scope.currencyText = t.register.choose

      $scope.confirm_password = ''
      $scope.ageOfConsent     = no

      $scope.chooseCurrency = (name, code) ->
        $scope.currencyText  = name
        $scope.form.currency = code

      $scope.validate = ->
        valid = (email) ->
          valid = /\S+@\S+\.\S+/
          valid.test email
        switch
          # all fields are filled
          when not _.every $scope.form
            $scope.alert = t.register.validation.all_filled
            no
          # email is ok
          when not valid $scope.form.email
            $scope.alert = t.register.validation.valid_email
            no
          # password longer than 7
          when not ($scope.form.password.length > 7)
            $scope.alert = t.register.validation.password_length
            no
          # password matches confirmation
          when not ($scope.form.password is $scope.confirm_password)
            $scope.alert = t.register.validation.password_confirmed
            no
          # age of consent checked
          when not $scope.ageOfConsent
            $scope.alert = t.register.validation.age_confirmed
            no
          else yes

      $scope.cancel = ->
        $modalInstance.dismiss('cancel')

      $scope.register = ->
        if $scope.validate()
          RegisterService.register $scope.form, (response) ->
            unless response.status is 0
              $scope.alert = response.message
            else
              $modalInstance.dismiss('ok')
              $modal.open
                animation: false
                templateUrl: 'loginmodal'
                controller: 'AuthModalCtrl'
                size: 'sm'
                resolve:
                  message: ->
                    t.auth.confirm_account

  RegisterModalCtrl.$inject = ['$scope', 't', 'RegisterService', 'CurrencyService', '$modalInstance', '$modal']

  angular
    .module 'azarthall.registration'
    .controller 'RegisterModalCtrl', RegisterModalCtrl
