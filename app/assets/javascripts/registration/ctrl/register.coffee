do (angular = window.angular, _ = window._) ->
  class RegisterCtrl
    constructor: ($scope, t, $modal) ->
      $scope.openRegistration = ->
        if window.location.pathname.match 'registration'
          backdrop = false
          keyboard = false
        else
          backdrop = true
          keyboard = true

        $modal.open
          animation: false
          templateUrl: 'registermodal'
          controller: 'RegisterModalCtrl'
          backdrop: backdrop
          keyboard: keyboard
          size: 'md'
          resolve:
            items: ->
              $scope.form

  RegisterCtrl.$inject = ['$scope', 't', '$modal']

  angular
    .module 'azarthall.registration'
    .controller 'RegisterCtrl', RegisterCtrl
