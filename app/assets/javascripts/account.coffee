#= require_tree ./account

do (angular = window.angular) ->
  angular
    .module('azarthall')
    .requires.push 'ui.bootstrap',
                   'azarthall.account'
