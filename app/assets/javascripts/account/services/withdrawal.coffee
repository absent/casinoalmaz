do (angular = window.angular) ->
  class WithdrawalService
    constructor: (@$http) ->

    methods: (callback) ->
      @$http.get '/account/payment_methods'
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка сервера'})

    request: (data, callback) ->
      @$http.post '/account/withdrawal', data
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка сервера'})

  WithdrawalService.$inject = ['$http']

  angular
    .module 'azarthall.account'
    .service 'WithdrawalService', WithdrawalService
