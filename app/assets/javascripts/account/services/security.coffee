do (angular = window.angular) ->
  class SecurityService
    constructor: (@$http) ->

    changePassword: (data, callback) ->
      @$http.post '/account/security/change_password', data
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка сервера'})

  SecurityService.$inject = ['$http']

  angular
    .module 'azarthall.account'
    .service 'SecurityService', SecurityService
