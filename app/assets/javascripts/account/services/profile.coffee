do (angular = window.angular) ->
  class ProfileService
    constructor: (@$http) ->
    get: (callback) ->
      @$http.get '/account/profile/info'
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка выполнения запроса'})
    save: (data, callback) ->
      @$http.post '/account/profile/info', data
        .success (response) =>
          (callback or angular.noop)(response)
        .error ->
          (callback or angular.noop)({status: 5, message: 'Ошибка сервера'})

  ProfileService.$inject = ['$http']

  angular
    .module 'azarthall.account'
    .service 'ProfileService', ProfileService
