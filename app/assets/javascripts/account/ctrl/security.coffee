do (angular = window.angular, _ = window._) ->
  class SecurityCtrl
    constructor: ($scope, t, SecurityService) ->

      $scope.password =
        current : ''
        new     : ''

      $scope.confirmPassword = ''

      @validate = ->
        switch
          when $scope.password.new.length < 7
            $scope.alert = t.register.validation.password_length
            no
          when $scope.new isnt $scope.password.confirm
            $scope.alert = t.register.validation.password_confirmed
            no
          else yes

      $scope.changePassword = =>
        if @validate()
          SecurityService.changePassword $scope.password, (response) ->
            if response.status is 0
              $scope.message = response.message
            else
              $scope.alert = response.message


  SecurityCtrl.$inject = ['$scope', 't', 'SecurityService']

  angular
    .module 'azarthall.account'
    .controller 'SecurityCtrl', SecurityCtrl
