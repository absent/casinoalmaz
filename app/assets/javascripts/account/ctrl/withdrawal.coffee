do (angular = window.angular, _ = window._, moment = window.moment) ->
  class WithdrawalCtrl
    constructor: ($scope, t, WithdrawalService, accountInfo) ->
      $scope.form =
        method: ''
        wallet: ''
        amount: ''

      @currency = accountInfo.currency

      @minAmounts =
        USD: 10
        RUB: 500
        EUR: 10
        GBP: 10

      $scope.minAmount = "#{@minAmounts[@currency]} #{@currency}"

      do ->
        WithdrawalService.methods (response) ->
          $scope.methods = response
          $scope.form.method = response[3]

      $scope.chooseMethod = (method) ->
        $scope.form.method = method

      $scope.validate = =>
        amount = parseInt $scope.form.amount, 10
        amount >= @minAmounts[@currency]

      $scope.withdrawalRequest = ->
        if $scope.validate()
          WithdrawalService.request $scope.form, (response) ->
            if response.status is 0
              $scope.message = response.message
            else
              $scope.alert   = response.message
        else
          $scope.alert = t.account.withdrawal.more_money


  WithdrawalCtrl.$inject = ['$scope', 't', 'WithdrawalService', 'accountInfo']

  angular
    .module 'azarthall.account'
    .controller 'WithdrawalCtrl', WithdrawalCtrl
