do (angular = window.angular, _ = window._, moment = window.moment) ->
  class ProfileCtrl
    constructor: ($scope, t, ProfileService, CurrencyService, CountryService) ->

      $scope.save_userinfo = ->
        if _.every $scope.dateBirth
          $scope.profile.date_of_birth = moment
              day  : parseInt($scope.dateBirth.day  , 10) + 1
              month: parseInt($scope.dateBirth.month, 10)
              year : parseInt($scope.dateBirth.year , 10)
            .toISOString()
        ProfileService.save $scope.profile, (response) ->
          unless response.status is 5
            window.location.reload()
          else
            $scope.alert   = response.message


      $scope.dateBirth =
        day  : null
        month: null
        year : null

      $scope.dateSelector =
        day_name  : t.account.profile.datepicker.day
        month_name: t.account.profile.datepicker.month
        year_name : t.account.profile.datepicker.year

      $scope.get_userinfo = ->

        ProfileService.get (response) ->

          unless response.status is 5

            $scope.profile = response

            unless response.date_of_birth is "1900-01-01T00:00:00"

              date_of_birth = moment response.date_of_birth, 'YYYY-MM-DDThh-mm-ss'
              $scope.dateBirth =
                day  : date_of_birth.format('D')
                month: date_of_birth.format('M') - 1
                year : date_of_birth.format('YYYY')

              $scope.dateSelector =
                day_name  : date_of_birth.format 'D'
                month_name: date_of_birth.format 'MMMM'
                year_name : date_of_birth.format 'YYYY'

            do ->
              CurrencyService.get (currencies) ->
                $scope.currencyList = currencies
                $scope.currencyText = _.result _.find(
                  currencies,
                  'code',
                  $scope.profile.currency
                ), 'name'

            do ->
              CountryService.get (countries) ->
                $scope.countryList = countries
                $scope.countryText = _.result _.find(
                  countries,
                  'code',
                  $scope.profile.country
                ), 'name'

          else

            $scope.alert = response.message

      $scope.get_userinfo()

      $scope.chooseCurrency = (name, code) ->
              $scope.currencyText  = name
              $scope.profile.currency = code

      $scope.chooseCountry = (name, code) ->
              $scope.countryText  = name
              $scope.profile.country = code

      $scope.computeYears = (currentYear) ->
          max_years = currentYear - 16
          min_years = currentYear - 70
          _.range min_years, max_years

      $scope.computeDays = (month, year) ->
          numOfDays = moment
              year: year
              month: month
          .daysInMonth()
          _.range 1, numOfDays + 1

      $scope.years  = $scope.computeYears moment().year()
      $scope.months = moment.months()
      $scope.days   = $scope.computeDays $scope.dateBirth.month, $scope.dateBirth.year

      $scope.changeDate = (key, value) ->
        ###
          Key is a string representing the value to change.
          Key may equal 'day', 'month' or 'year'.
          In case key is 'month', an array of format
          [int month_number, str month_name]
          should be supplied as value.
          In all other cases, value is treated as integer.
          The function will perform a check after changing month or year,
          to ensure the number of days in month is valid for this month/year.
        ###

        dayCheck = ->
          if $scope.days[$scope.dateBirth.day - 1] is undefined
            $scope.dateSelector.day_name = t.account.profile.datepicker.day
            $scope.dateBirth.day = null

        switch key
          when 'day'
              $scope.dateBirth.day           = value
              $scope.dateSelector.day_name   = value
              yes
          when 'month'
              $scope.dateBirth.month         = value[0]
              $scope.dateSelector.month_name = value[1]

              $scope.days = $scope.computeDays value[0], $scope.dateBirth.year
              dayCheck()
          when 'year'
              $scope.dateBirth.year         = value
              $scope.dateSelector.year_name = value

              $scope.days = $scope.computeDays $scope.dateBirth.month, value
              dayCheck()
          else
            no


  ProfileCtrl.$inject = ['$scope', 't', 'ProfileService', 'CurrencyService', 'CountryService']

  angular
    .module 'azarthall.account'
    .controller 'ProfileCtrl', ProfileCtrl
