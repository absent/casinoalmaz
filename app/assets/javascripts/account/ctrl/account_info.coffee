do (angular = window.angular, _ = window._) ->
  class AccountInfoCtrl
    constructor: ($scope, t, accountInfo) ->
      $scope.user =
        username: accountInfo.username
        balance : accountInfo.balance
        currency: accountInfo.currency

  AccountInfoCtrl.$inject = ['$scope', 't', 'accountInfo']

  angular
    .module 'azarthall.account'
    .controller 'AccountInfoCtrl', AccountInfoCtrl
