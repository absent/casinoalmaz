//= require jquery-2.1.4
//= require lodash
//= require moment-with-locales
//= require angular
//= require angular-animate
//= require angular-i18n
//= require angular-ui-bootstrap
//= require angular-ui-bootstrap-tpls

(function(angular, moment) {
    'use strict';

    config.$inject = ['$provide', '$httpProvider'];
    function config($provide, $httpProvider) {
        $provide.constant('t', window.onebett_locale);

        // Global timeout
        $httpProvider.interceptors.push(function() {
            return {
                request: function(config) {
                    // Template file regexp
                    var RE_TPL = /\.html?$/;

                    // In case when requesting not a template
                    // and no timeout in config =
                    // set default timeout 30 seconds
                    if (!RE_TPL.exec(config.url) &&
                        config.timeout === void 0) {
                        config.timeout = 30000;
                    }

                    return config;
                }
            };
        });
    }
    run.$inject = ['lang', 't', '$rootScope'];
    function run(lang, t, $rootScope) {
         moment.locale(lang);
         moment.relativeTimeThreshold('s', 60);
         moment.relativeTimeThreshold('m', 60);
         moment.relativeTimeThreshold('h', 24);
         $rootScope.t = t;

    }


    var modules = [
        'ngAnimate',
        'ui.bootstrap',
        'angular-i18n'
    ];

    angular
	.module('azarthall', modules)
        .config(config)
	.run(run)
})(window.angular, window.moment);
