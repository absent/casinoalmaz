module AzartHall
  class Registration
    def self.save_user(session, params, host = 'http://qbitcasino.com')
        params[:application] = 'QbitApp'
        # params[:DealerKiosk] = Referal.get_ksk(utm) if Referal.check_referal(utm)

        if params[:username].nil? or params[:email].nil?
          return {:status => data['Status'], :message => I18n.t('frontend.register.response.data_exists')}
        end

        response = AzartHall::Api.post 'Account/Register', params
        return { :status => -2, :message => I18n.t('frontend.register.response.error') } if response.code != '200'
        data = JSON.parse response.body
        case data['Status']
          when 0, 6
            approveType = data['ApproveType'].to_i + 1
            message_status = send_verifymessage(data['ApproveContact'], params[:Password], params[:SendPassword] == "true", approveType, data['VerificationKey'], host)
            {:status => 0 }
          when 1
            {:status => data['Status'], :message => I18n.t('frontend.register.response.data_exists')}
          when 2
            {:status => data['Status'], :message => I18n.t('frontend.register.response.user_exists')}
          when 3
            {:status => data['Status'], :message => I18n.t('frontend.register.response.error')}
          when 4
            {:status => data['Status'], :message => I18n.t('frontend.register.response.bad_params')}
          else
            {:status => -1, :message => I18n.t('frontend.register.response.error')}
        end
    end

    def self.validate_user(params)
      return { :Status => -3 } if !params[:k].present?

      response = AzartHall::Api.post 'Account/Verify', { :VerificationKey => params[:k], :Approve => true }
      return { :Status => -2 } if response.code != '200'
      return JSON.parse response.body
    end

    def self.send_verifymessage(login, pass, sendpass, type, key, host)
      if type == 1
        request_params = { :email => login, :url => "#{host}/registration/validate?k=#{key}" }
        if (sendpass)
          request_params[:pass] = pass
        end

        response = AzartHall::NotificationsApi.get 'siteregaz', request_params
        return -1 if response.code != '200'

        data = JSON.parse response.body
        return -1 if data['ErrorCode'] != 0
        return 0
      else
        return -2
      end
    end
  end
end
