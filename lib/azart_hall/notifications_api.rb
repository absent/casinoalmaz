module AzartHall
  class NotificationsApi
    API_URL = "http://errors.onebett.com/"

    def self.get(type, fields)
      Net::HTTP.get_response build_uri type, fields
    end

    def self.sms(number, text)
      uri = URI.parse API_URL + "SendSms?number=#{number}&text=#{URI::encode_www_form_component(text)}"
      Net::HTTP.get_response uri
    end

    def self.sms_session_check(session)
      if session[:sms_last_send].nil?
        session[:sms_last_send] = Time.now.to_i
        session[:sms_first_send] = Time.now.to_i
        session[:sms_count] = 1
        return 0
      end

      currentTime = Time.now.to_i
      if currentTime - session[:sms_last_send] < 60
        return 1
      end

      if currentTime - session[:sms_first_send] > 60 * 60
        session[:sms_first_send] = currentTime
        session[:sms_count] = 0
      end

      if session[:sms_count] > 4
        return 2
      end

      session[:sms_count] += 1
      return 0
    end

    def self.build_uri(type, fields)
      message = ''
      delim = '';
      fields.each do |key, value|
        messagePart = delim + "#{key}=#{value}"
        message += URI::encode_www_form_component(messagePart)
        delim = '|'
      end

      get_uri "SendError?errorType=#{type}&message=#{message}&type=5"
    end

    def self.get_uri(path)
      sep = path.index('?') ? '&' : '?'
      if path.index('lang')
        URI.parse API_URL + path
      else
        URI.parse API_URL + path + sep + "lang=#{Locale.cur}"
      end
    end
  end
end
