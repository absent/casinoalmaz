module AzartHall
  class Auth
    def self.check_auth(session, login, password)
      post_params = {
          username: login,
          password: password,
          application: 'WebSiteApp'
      }

      response = AzartHall::Api.post 'Account/Authorize', post_params
      return { :status => 3, :message => I18n.t('auth.server_error') } if response.code != '200'

      # 0 - Success operation
      # 1 - User not found
      # 2 - User is blocked,and BlockTimeOut store time to unblocked
      # 3 - User be blocked, and BlockTimeOut store time to unblocked
      # 4 - incorrect data, in BlockTimeOut store time to unblocked, Attempts count attps before block

      data = JSON.parse response.body
      return { :status => 3, :message => I18n.t('auth.server_error') } if data['UserId'].nil? # never trust data
      case data['Status']
        when 0
          session[:username] = login
          session[:guid] = data['UserId']
          session[:user_key] = data['UserKey']
          {
              :status => 0,
              :user_data => {
                  :username => login,
                  :guid => data['UserId'],
                  :balance_info => get_balance(session)
              },
              :message => 'Ok'
          }
        when 1
          { status: 1, message: I18n.t('auth.no_data') }
        when 2, 3
          { :status => 1,
            :message => I18n.t('auth.blocked',
                                 :timeout => I18n.t('datetime.distance_in_words.x_minutes',
                                                    count: data["BlockTimeout"]))

          }
        when 4
          { :status => 1,
            :message => I18n.t('auth.fail',
                                 :attempts => I18n.t('attempts', count: data["Attempts"]),
                                 :timeout => I18n.t('datetime.distance_in_words.x_minutes',
                                                    count: data["BlockTimeout"]))


          }
        when 5
          { :status => 1,
            :message => I18n.t('auth.not_confirmed')
          }
        when 6
            resp = AzartHall::Api.get "Account/AccountPreRegistrationCheckPin?login=#{login}&numberReceived=#{password}"
            return { :status => 3, :message => I18n.t('auth.server_error') } if resp.code != '200'
            pre_data = JSON.parse resp.body
            case pre_data['StatusCode']
              when 0
                  if pre_data['IsRegistered'] == true
                    return {:status => 7, :login => login, :amount => pre_data['Amount']}
                  else
                    return {:status => 6, :login => login, :amount => pre_data['Amount']}
                  end
              else
                {
                    :status => 1,
                    :message => I18n.t('auth.fail')
                }
            end
        else
          { :status => 3, :message => I18n.t('auth.server_error') }
      end
    end

    def self.get_userid(login)
      application = "QBitApp"
      response = AzartHall::Api.get "Account/UserKey?Application=#{application}&Username=#{login}"
      return { :status => 3, :message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body

      return { :status => 1, :message => I18n.t('auth.no_data') } if data['Status'] == 1

      return { :status => 0, :userId => data['UserId'] }
    end

    def self.verify_user(login)
      response = self.get_userid login
      if response[:status] != 0
        return { :Status => response[:status], :Message => response[:message] }
      end

      userId = response[:userId]

      response = AzartHall::Api.get "Account/VerificationKey?UserId=#{userId}"
      return { :Status => 3, :Message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body
      return { :Status => 3, :Message => I18n.t('auth.server_error') } if data['Status'] != 0

      post_params = {
        :VerificationKey => data['VerificationKey'],
        :Approve => true
      }

      response = AzartHall::Api.post "Account/Verify", post_params
      return { :Status => 3, :Message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body
      return { :Status => 3, :Message => I18n.t('auth.server_error') } if data['Status'] != 0

      return { :Status => 0 }
    end

    def self.create_newpassword(userId)
      post_params = {
          UserId: userId,
          CheckOldPassword: false,
          GenerateNewPassword: true
      }

      response = AzartHall::Api.post "Account/ChangePassword", post_params
      return { :status => 3, :message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body
      return { :status => 3, :message => I18n.t('auth.server_error') } if data['Status'] != 0

      return { :status => 0, :newPassword => data['NewPassword'] }
    end

    def self.set_newpassword(userId, newpassword)
      post_params = {
          UserId: userId,
          CheckOldPassword: false,
          GenerateNewPassword: false,
          NewPassword: newpassword
      }

      response = AzartHall::Api.post "Account/ChangePassword", post_params
      return { :status => 3, :message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body
      return { :status => 3, :message => I18n.t('auth.server_error') } if data['Status'] != 0

      return { :status => 0, :newPassword => data['NewPassword'] }
    end

    # type
    # 0 - any suitable method
    # 1 - email
    # 2 - phone
    def self.send_newpassword(login, type, session)
      response = self.get_userid(login)

      if (response[:status] != 0)
        return response
      end

      userId = response[:userId]

      response = AzartHall::Api.get "Account/User/?UserId=#{userId}"
      return { :status => 3, :message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body
      return { :status => 3, :message => I18n.t('auth.server_error') } if data['Status'] != 0

      contact = ''
      if type == 0
        if data['Email'].present?
          type = 1
          contact = data['Email']
        elsif data['PhoneNumber'].present?
          type = 2
          contact = data['PhoneNumber']
          contact = contact.tr('^0-9', '')
        else
          return { :status => 2, :message => I18n.t('auth.no_data') }
        end
      elsif type == 1
        if data['Email'].present?
          contact = data['Email']
        else
          return { :status => 2, :message => I18n.t('auth.no_data') }
        end
      end

      if session[:reset_password_contact].nil? || session[:reset_password_contact] != contact
        response = self.create_newpassword(userId)

        if response[:status] != 0
          return response
        end

        session[:reset_password_contact] = contact
        session[:reset_password_password] = response[:newPassword]
      else
        response = self.set_newpassword(userId, session[:reset_password_password])

        if response[:status] != 0
          return response
        end
      end

      newPassword = session[:reset_password_password]

      if type == 1
        p contact
        p newPassword
        response = AzartHall::NotificationsApi.get 'newpassqbit', { :email => contact, :pass => newPassword }
        return { :status => 5, :message => I18n.t('auth.server_error') } if response.code != '200'

        data = JSON.parse response.body
        p data
        return { :status => 6, :message => I18n.t('auth.notification_server_error') } if data['ErrorCode'] != 0

        return { :status => 0, :message => I18n.t('auth.newpassword_send_email') }
      else
        check = AzartHall::NotificationsApi.sms_session_check session

        if check == 0
          response = AzartHall::NotificationsApi.sms contact, I18n.t('account.sms_messages.password_reset', password: newPassword)

          return { :status => 5, :message => I18n.t('auth.server_error') } if response.code != '200'

          data = JSON.parse response.body
          return { :status => 6, :message => I18n.t('auth.notification_server_error') } if data['ErrorCode'] != 0

          return { :status => 0, :message => I18n.t('account.profile.sms_success') }
        elsif check == 1
          return { :status => 999, :message => I18n.t('account.profile.sms_error_fast') }
        else
          return { :status => 998, :message => I18n.t('account.profile.sms_error_limit') }
        end
        return { :status => 4, :message => I18n.t('auth.phone_error') }
      end
    end

    def self.resend_email(email, host)
      application = "QBitApp"
      response = AzartHall::Api.get "Account/UserKey?Application=#{application}&Username=#{email}"
      return { :status => 3, :message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body

      return { :status => 1, :message => I18n.t('auth.no_data') } if data['Status'] == 1

      userId = data['UserId']

      response = AzartHall::Api.get "Account/VerificationKey/?UserId=#{userId}"
      return { :status => 3, :message => I18n.t('auth.server_error') } if response.code != '200'

      data = JSON.parse response.body
      return { :status => 3, :message => I18n.t('auth.server_error') } if data['Status'] != 0 && data['Status'] != 3

      status = AzartHall::Registration.send_verifymessage(email, '', false, 1, data['VerificationKey'], host)
      case status
        when 0
          return { :status => 0, :message => I18n.t('auth.key_send_success') }
        else
          return { :status => 4, :message => I18n.t('auth.key_send_error') }
      end
    end

    # TODO: Check this method after refresh docs
    def self.get_balance(session)
      response = AzartHall::Api.get "Account/Balance?UserId=#{session[:guid]}"
      return I18n.t 'auth.server_error' if response.code != '200'

      data = JSON.parse response.body

      if data['Status'] == 0
        session[:balance] = data['Balance']
        session[:currency] = data['CurrencyCode']
        { success: 0, balance: data['Balance'], currency: data['CurrencyCode'] }
      else
        { success: 1, message: "#{I18n.t 'auth.server_error'}" }
      end
    end

    def self.clean_user_session(session)
      session[:username] = nil
      session[:guid] = nil
      session[:user_key] = nil
      session[:balance] = nil
      session[:currency] = nil
    end
  end
end
