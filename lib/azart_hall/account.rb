module AzartHall
  class Account

    def self.get_userinfo(guid)
      response = AzartHall::Api.get "Account/User?UserId=#{guid}"
      return I18n.t 'auth.server_error' if response.code != '200'

      data = JSON.parse response.body
    end

    def self.set_userinfo(guid, params)
      response = AzartHall::Api.post "Account/User/?UserId=#{guid}", params
      if response.code == '200'
        data = JSON.parse response.body
        case data['Status']
        when 0
          {
            :status => data['Status'],
            :message => "#{I18n.t 'account.profile.save_success'}"
          }
        when 1
          {
            :status => data['Status'],
            :message => "#{I18n.t 'account.profile.save_not_found'}"
          }
        when 2
          {
            :status => data['Status'],
            :message => "#{I18n.t 'account.profile.save_db_error'}"
          }
        else
          {
            :status => data['Status'],
            :message => "#{I18n.t 'account.profile.undefined_error'}"
          }
        end
      else
        {
          :status => 4,
          :message => "#{I18n.t 'auth.server_error'}"
        }
      end
    end

    def self.change_password(params)
      response = AzartHall::Api.post "Account/ChangePassword", params

      if response.code == '200'
        data = JSON.parse response.body
        case data['Status']
        when 0
          {
            :status => 0,
            :message => "#{I18n.t 'frontend.account.security.success'}"
          }
        when 2
          {
            :status => 2,
            :message => "#{I18n.t 'frontend.account.security.incorrect_pass'}"
          }
        else
          {
            :status => data['Status'],
            :message => "#{I18n.t 'auth.server_error'}",
            :data => data
          }
        end
      else
        {
          :status => 4,
          :message => "#{I18n.t 'auth.server_error'}"
        }
      end
    end

    def self.deposit(params)
      response = Azarthall::Api.post 'Transactions/Deposit', params

      if response.code == '200'
        data = JSON.parse response.body
        if (data['Status'] != 0)
          {
              :status => -2,
              :message => I18n.t('auth.server_error'),
              :data => data
          }
        else
          {
              :status => 0,
              :message => I18n.t('frontend.account.withdrawal.success')
          }
        end
      else
        {
            :status => 0,
            :message => I18n.t('frontend.account.withdrawal.success')
        }
      end
    end

    def self.withdrawal_request(params)
      response = AzartHall::Api.post 'Requests', params

      if response.code == '200'
        data = JSON.parse response.body
        if (data['Status'] != 0) or (data['Status'] != -1)
          {
            :status => -2,
            :message => I18n.t('auth.server_error'),
            :data => data
          }
        else
          {
            :status => 0,
            :message => I18n.t('frontend.account.withdrawal.success')
          }
        end
      else
        {
          :status => -2,
          :message => I18n.t('auth.server_error')
        }
      end
    end

    def self.get_payment_methods(guid)
      return ['2btx']

      response = AzartHall::Api.get "Transactions/PaymentMethods?UserId=#{guid}"

      if response.code == '200'
        data = JSON.parse response.body
        if data['Status'] != 0
          {
            :status => -2,
            :message => I18n.t('auth.server_error')
          }
        else
          data['Methods'].map { |m| m['Description'] }
        end
      else
        {
          :status => -2, :message => I18n.t('auth.server_error')
        }
      end
    end
  end
end
