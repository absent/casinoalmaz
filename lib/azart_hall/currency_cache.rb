module AzartHall
  class CurrencyCache
    def self.update
      I18n.available_locales.each do |lang|
        self.insert_currencies(lang.to_s, AzartHall::Api.get_currencies(lang.to_s))
      end
    end

    def self.insert_currencies(lang, attrs)
      if attrs.present?
        #Delete all currencies
        Currency.where(country:lang).delete_all
        #Insert currencies in table
        attrs.each do |currency|
          n_currency = Currency.new
          n_currency.country = lang
          n_currency.code = currency["Code"].strip
          n_currency.name = currency["Name"].strip
          n_currency.save
        end
      end
    end
  end
end
