module AzartHall
  class Gcache
    API_URL = "http://bl.onebett.com/api/leader/GetProdJson"
    IMG_HOST = "flashslots.s3.amazonaws.com"

    def self.cache
      uri = URI.parse API_URL
      response = Net::HTTP.get_response uri
      data = JSON.parse(response.body) if response.kind_of? Net::HTTPSuccess

      data['applications'].each do |app|
        if app[0] == "loader"
          next
        end
        if app[0] == "opfl_haxe_loader"
          next
        end
        puts "app:"+ app[0]

        a = Game.find_by_id_name(app[0])
        a = Game.new if a.nil?
        a.id_name = app[0]
        a.app = app[1]['app'][0]
        a.name = app[1]['name'][0]['en']
        a.img = /thumb.(.*).png/.match(app[1]['preview'])[1]
        puts a.img
        a.type_game = app[1]['type']
        a.loader = data['applications'][app[1]['loader']]['app'][0]
        a.save
        Net::HTTP.start(IMG_HOST) do |http|
          puts app[1]['preview']
          resp = http.get("/#{app[1]['preview']}")
          open("#{Rails.root}/public/" + app[1]['preview'], "wb") do |file|
            file.write(resp.body)
          end
          puts "convert \"#{Rails.root}/public/" + app[1]['preview'] +"\"" + " \"#{Rails.root}/public/images/" + a.img + ".jpg\""
          system("convert \"#{Rails.root}/public/" + app[1]['preview'] +"\""+ " \"#{Rails.root}/public/images/" + a.img + ".jpg \"")
        end
      end
      end

  end
end
