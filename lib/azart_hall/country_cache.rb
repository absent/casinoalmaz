module AzartHall
  class CountryCache
    def self.update
      I18n.available_locales.each do |lang|
        insert_countries(lang.to_s, AzartHall::Api.get_countries(lang.to_s))
      end
    end

    def self.insert_countries(lang, attrs)
      if attrs.present?
        Country.where(lang_code: lang).delete_all
        #generate aliases in alias table
        attrs.each do |country|
          unless Country.exists?(country["Code"])
            n_alias = Country.new
            n_alias.code = country["Code"].strip
            n_alias.name = country["Name"]
            n_alias.lang_code = lang
            n_alias.phonecode = country["PhoneCode"]
            n_alias.save
          end
        end
      end
    end
  end
end
