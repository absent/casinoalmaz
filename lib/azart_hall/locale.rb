module AzartHall
  include Rails.application.routes.url_helpers

  class Locale
    # Initialise locale
    def self.init(cookie, request, params)
      if params[:lng] != ''
        locale = params[:lng]
        if available? locale
          apply_locale locale
          return locale
        end
      end
      locale = extract_locale_from_cookie cookie
      if available? locale
        apply_locale locale
        return locale
      else
        locale = extract_locale_from_browser request
        locale = :ru unless available? locale
        apply_locale locale
        return locale
      end
    end

    #
    def self.available?(locale)
      I18n.locale_available? locale
    end

    # Get current locale
    def self.cur
      I18n.locale.to_s
    end

    private

    # Get locale from subdomain
    def self.extract_locale_from_url(request)
      res = request.subdomain.scan(/^[a-z]{2}/).first
      if res != nil
        res = res
      end
      return res
    end

    def self.extract_locale_from_cookie(cookie)
      res = cookie[:lng]
      if res != nil
        res = res.to_sym
      end
      return res
    end

    # Get first available locale from user's browser
    def self.extract_locale_from_browser(request)
      begin
        request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
      rescue
        'ru'
      end
    end

    # Apply locale to system
    def self.apply_locale(locale)
      I18n.locale = locale
    end
  end
end
