module AzartHall
  class Crypto
    API_URL = "http://2btx.com:8080/"

    def self.get(path)
      Net::HTTP.get_response get_uri path
    end

    def self.post(path, fields)
      Net::HTTP.post_form get_uri(path), fields
    end

    def self.receive(userId, callback)
      response = AzartHall::Crypto.get "receive?userId=#{userId}&systemCallback=#{callback}"
      if response.code == '200'
        data = JSON.parse response.body
        return data
      end
      return {
          :status => -1,
          :message => I18n.t('auth.server_error')
      }
    end

    private

    def self.get_uri(path)
      uri = URI.parse API_URL + path
      p uri
      return uri
    end
  end
end