module AzartHall
  class Api
    API_URL = 'http://api.onebett.com/'
    #API_URL = 'http://192.168.89.237:43137/'

    def self.get(path)
      Net::HTTP.get_response get_uri path
    end

    def self.post(path, fields)
      Net::HTTP.post_form get_uri(path), fields
    end

    def self.get_currencies(lang)
      response = get "Misc/Currency/?lang=#{lang}"
      return if response.code != '200'
      data = JSON.parse response.body
    end

    def self.get_countries(lang)
      response = get "Misc/Countries/?lang=#{lang}&filter=true"
      return if response.code != '200'
      data = JSON.parse response.body
    end

    def self.encrypt(str, key)
      des = OpenSSL::Cipher::Cipher.new('des3')
      des.encrypt
      des.padding = 1
      des.key = key
      enc = des.update(str) + des.final
      Base64.strict_encode64(enc)
    end

    private

    def self.get_uri(path)
      sep = path.index('?') ? '&' : '?'
      if path.index('lang')
        URI.parse API_URL + path
      else
        URI.parse API_URL + path + sep + "lang=#{Locale.cur}&v=2"
      end
    end

    def self.to_alias(string)
      string.gsub(/\W/, ' ').strip.gsub(/(\s+)/, ' ').gsub(/\s/,'-').downcase
    end

    def self.to_transalias(string)
      to_alias(I18n.transliterate(string))
    end
  end
end
