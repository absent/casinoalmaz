namespace :currency_cache  do
  desc "Precache currencies"
  task :update => :environment do
    AzartHall::CurrencyCache.update
  end
end
