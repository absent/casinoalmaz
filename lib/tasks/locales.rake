namespace :locales  do
  desc "Update locales"
  task :update => :environment do
    I18n.available_locales.each do |locale|

      I18n.locale = locale
      I18n.locale = I18n.default_locale if I18n.t('frontend').class != Hash

      File.open("public/lang/#{locale}.js",'w') do |f|
        f.write("'use strict';")
        f.write('var onebett_locale = ' + I18n.t('frontend').to_json + ';')
      end
    end
  end
end
