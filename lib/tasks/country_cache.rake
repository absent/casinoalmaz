namespace :country_cache do
  desc "Precache countries"
  task :update => :environment do
    AzartHall::CountryCache.update
  end
end
