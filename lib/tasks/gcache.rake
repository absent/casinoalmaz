namespace :gcache  do
  desc "Precache games"
  task :update => :environment do
    AzartHall::Gcache.cache
  end
end
