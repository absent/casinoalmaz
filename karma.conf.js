'use strict';

module.exports = function(config) {
    config.set({
        singleRun: true,
        browsers: ['PhantomJS'],
        frameworks: ['jasmine'],
        reporters: ['progress', 'junit'],
        colors: false,
        preprocessors: {
            'tests/fixtures/*.json': ['html2js']
        },
        files: [
            'vendor/assets/javascripts/lodash.js',
            'vendor/assets/javascripts/jquery-2.1.1.js',
            'vendor/assets/javascripts/angular.js',
            'vendor/assets/javascripts/angular-mocks.js',
            'vendor/assets/javascripts/angular-cookies.js',

            'tests/unit/**/*.test.js',

            'tests/fixtures/*.json'
        ],
        junitReporter: {
            outputFile: 'tests/karma.xml',
            suite: 'unit'
        }
    });
};
