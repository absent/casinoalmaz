require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/rvm'
require 'mina/unicorn'



set :rails_env,       'production'

set :domain,          '45.32.234.113'
set :port,            22
set :deploy_to,       '/home/azarthall/www'
set :user,            'azarthall'

set :repository,      'git@gitlab.com:absent/casinoalmaz.git'
set :unicorn_pid,			"#{deploy_to}/shared/pids/unicorn.pid"
set :branch,          'master'
set :shared_paths,    %w(config/database.yml config/secrets.yml config/keys.yml config/payment.yml public/system log)



task :environment do
  invoke :'rvm:use[ruby-2.1.5@default]'
end



task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]

  queue! %[touch "#{deploy_to}/shared/config/database.yml"]
  queue  %[echo "-----> Be sure to edit 'shared/config/database.yml'."]

  queue! %[touch "#{deploy_to}/shared/config/secrets.yml"]
  queue %[echo "-----> Be sure to edit 'shared/config/secrets.yml'."]

  queue! %[touch "#{deploy_to}/shared/config/keys.yml"]
  queue %[echo "-----> Be sure to edit 'shared/config/keys.yml'."]

  queue! %[touch "#{deploy_to}/shared/config/payment.yml"]
  queue %[echo "-----> Be sure to edit 'shared/config/payment.yml'."]

  queue! %[mkdir -p "#{deploy_to}/shared/pids/"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/pids"]

  queue! %[mkdir -p "#{deploy_to}/shared/sockets/"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/sockets"]

  queue! %[mkdir -p "#{deploy_to}/shared/public/system/"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/public/system"]
end



task :deploy => :environment do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    
    invoke :'rails:db_migrate'
    
    queue! %[#{rake} locales:update]
    #invoke :'rails:assets_precompile'
    #queue! %[#{rake} gcache:update]
    queue! %[#{rake} currency_cache:update]
    queue! %[#{rake} country_cache:update]
    invoke :'deploy:cleanup'

    to :launch do
      invoke :'unicorn:restart'
    end
  end
end
