Rails.application.routes.draw do

  get 'ru' => 'locale#switch', :code => 'ru'
  get 'en' => 'locale#switch', :code => 'en'

  #account routes
  namespace :account do
    get '/' => '#profile'
    #profile routes
    get 'profile' => :profile
    get 'profile/info' => :get_userinfo
    post 'profile/info' => :save_userinfo
    #security routes
    get 'security' => :security
    post 'security/change_password' => :change_password
    #payment routes
    get 'deposit' => :deposit
    get 'deposit_2btx' => :deposit_2btx
    get 'withdrawal' => :withdrawal
    get 'payment_methods' => :payment_methods
    post 'withdrawal' => :withdrawal_request
  end

  root 'games#index'
  get 'get' => 'games#get'

  get 'top' => 'top#get'
  get 'currencies' => 'currency#get'
  get 'countries' => 'country#get'

  #registration routes
  get 'registration'  => 'registration#index'
  get 'registration/validate' => 'registration#validate'
  get 'register/success' => 'registration#success_registration'
  post 'register' => 'registration#registration'

  #auth routes
  get 'login'  => 'auth#index'
  get 'auth' => 'auth#login'
  get 'logout' => 'auth#logout'
  get 'forgot_password' => 'auth#forgot_password'

  #games routes
  get 'games' => 'games#index'
  get 'games/(:category)' => 'games#index'
  get 'games/get' => 'games#get'
  get 'game/(:id)' => 'game#index'

  #plugin routes
  get 'plugins' => 'plugin#index'
  
  #admin routes
  namespace :admin do
    get 'login/index'
    get 'login/logout'
    get 'login/check_login'
    get 'users/get'
    get 'users/create'
    get 'users/update'
    get 'users/remove'
  end
end
