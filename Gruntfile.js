'use strict';

module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        karma: {
            unit: {
                configFile: 'karma.conf.js'
            }
        },
        protractor: {
            e2e: {
                configFile: 'protractor.conf.js'
            }
        },
        exec: {
            rails: {
                cmd: 'rails server'
            },
            locales: {
                cmd: 'bin/rake locales:update'
            },
            precache: {
                cmd: 'bin/rake gcache:update --trace && bin/rake currency_cache:update && bin/rake country_cache:update'
            },
            migrate_db: {
                cmd: 'bin/rake db:migrate'
            },
            create_db: {
                cmd: 'bin/rake db:create'
            },
            kill: {
              cmd: 'sudo kill $(cat tmp/pids/server.pid)'
            },
        },
        watch: {
            main: {
                files: ['app/assets/javascripts/**/*.js',
                        'app/assets/javascripts/**/*.coffee',
                        'app/assets/stylesheets/**/*.scss',
                        'app/views/**/*.erb',
                        'public/**/*.html',
                        'public/**/*.css',
                        'public/**/*.png',
                        'public/**/*.jpg'
                ],
                tasks: [],
                options: {
                    livereload: true
                }
            },
            locales: {
                files: ['config/locales/*.yml'],
                tasks: ['exec:locales'],
                options: {
                    livereload: true
                }
            }
        },
        concurrent: {
            options: {
              logConcurrentOutput: true
            },
            serve: ['exec:rails', 'watch']
        }
    });
    grunt.registerTask('clear', function () {
        grunt.log.writeln('Checking if server is already running...');
        if (grunt.file.exists('tmp/pids/server.pid')) {
            grunt.log.writeln('Pidfile found, let\'s kill the process:');
            grunt.task.run('exec:kill');
        } else {
            grunt.log.writeln('No pidfile found, moving on...');
        }
    });
    grunt.registerTask('dbsetup', ['exec:create_db', 'exec:migrate_db']);
    grunt.registerTask('server', ['clear', 'exec:precache', 'exec:locales', 'concurrent']);

    // rails-like aliases
    grunt.registerTask('s', ['clear', 'concurrent']);
    grunt.registerTask('db', ['dbsetup']);
};
