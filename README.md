# Awesome grunt features that you can use

## Installation

### Grunt and packages
To start using this simple and cool setup, cd into project folder and type
```
npm install -g grunt-cli
npm install
```

### Livereload

1. Install chrome extension from here
https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei
2. Go to the page you want to livereload and click the plugin icon. If the little dot in the center of the icon fills or the page reloads, you're done.


## Features

### Database setup in one command
```
grunt dbsetup
```
Initial db setup. Runs create and migrate db rake tasks.

### Advanced server start
```
grunt server
```
Updates precache and locales, before running the server + runs file watcher and reloads your page.

*These grunt commands also have shorter aliases (much like in rails)* `grunt s` *and* `grunt db`

### Livereload (Chrome only)

The most delicious feature.
- watches for changes in views/stylesheets/scripts/locales
- regenerates locales after locale configs change
- reloads the page after frontend files change
