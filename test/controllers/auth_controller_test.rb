require 'test_helper'

class AuthControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get forgot_password" do
    get :forgot_password
    assert_response :success
  end

  test "should get forgot_password_window" do
    get :forgot_password_window
    assert_response :success
  end

  test "should get resend_email_window" do
    get :resend_email_window
    assert_response :success
  end

  test "should get forgot_login" do
    get :forgot_login
    assert_response :success
  end

  test "should get resend_email" do
    get :resend_email
    assert_response :success
  end

  test "should get logout" do
    get :logout
    assert_response :success
  end

end
