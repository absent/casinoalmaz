require 'test_helper'

class AccountControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get profile" do
    get :profile
    assert_response :success
  end

  test "should get deposit" do
    get :deposit
    assert_response :success
  end

  test "should get withdrawal" do
    get :withdrawal
    assert_response :success
  end

end
