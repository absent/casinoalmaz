require 'test_helper'

class FrontEndControllerTest < ActionController::TestCase
  test "should get get_currency" do
    get :get_currency
    assert_response :success
  end

  test "should get get_params" do
    get :get_params
    assert_response :success
  end

  test "should get update_balance" do
    get :update_balance
    assert_response :success
  end

end
