require 'test_helper'

class RegistrationControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get user_registration" do
    get :user_registration
    assert_response :success
  end

  test "should get success_registration" do
    get :success_registration
    assert_response :success
  end

  test "should get validate" do
    get :validate
    assert_response :success
  end

  test "should get email_registration" do
    get :email_registration
    assert_response :success
  end

  test "should get check_reg_user" do
    get :check_reg_user
    assert_response :success
  end

end
