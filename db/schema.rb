# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150805101137) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string   "login"
    t.string   "password"
    t.integer  "role"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.string   "code",          limit: 3
    t.string   "phonecode"
    t.string   "country_alias", limit: 25
    t.string   "lang_code",     limit: 3
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "code",       limit: 3
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "country",    limit: 3
  end

  create_table "games", force: :cascade do |t|
    t.string   "id_name"
    t.string   "name"
    t.string   "loader"
    t.string   "type_game"
    t.string   "category"
    t.string   "img"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "app"
  end

  add_index "games", ["id_name"], name: "index_games_on_id_name", using: :btree

  create_table "page_urls", force: :cascade do |t|
    t.string   "url"
    t.boolean  "active",     default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "page_name"
    t.integer  "page_type"
  end

  add_index "page_urls", ["url"], name: "index_page_urls_on_url", unique: true, using: :btree

end
