class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :code, limit: 3
      t.string :name
      t.timestamps
    end
  end
end
