class CreatePageUrls < ActiveRecord::Migration
  def change
    create_table "page_urls", force: true do |t|
        t.string   "url"
        t.boolean  "active",     default: true
        t.datetime "created_at"
        t.datetime "updated_at"
        t.string   "page_name"
        t.integer  "page_type"
      end

      add_index "page_urls", ["url"], name: "index_page_urls_on_url", unique: true, using: :btree
  end
end
