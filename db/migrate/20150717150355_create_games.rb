class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :id_name
      t.string :name
      t.string :loader
      t.string :type
      t.string :category
      t.string :img
      t.timestamps
    end
  end
end
