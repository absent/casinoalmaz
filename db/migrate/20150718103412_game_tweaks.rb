class GameTweaks < ActiveRecord::Migration
  def change
    add_index :games, :id_name
    rename_column :games, :type, :type_game
  end
end
