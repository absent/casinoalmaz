class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name
      t.string :code, limit: 3
      t.string :phonecode
      t.string :country_alias, limit: 25
      t.string :lang_code, limit: 3

      t.timestamps
    end
  end
end
