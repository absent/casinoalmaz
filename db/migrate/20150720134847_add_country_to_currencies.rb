class AddCountryToCurrencies < ActiveRecord::Migration
  def change
    add_column :currencies, :country, :string, :limit => 3
  end
end
